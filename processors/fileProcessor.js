import R from 'ramda';

let processingFile = '';
let priority = 'none';
let numberOfFiles = 0;
let timeout = 100;

// queues
let pdfs = [];
let htmls = [];
let firstFile = '';

export function addToTheQueue(file) {
  // We need a reference to the first file, to know which type arrived first
  if (priority === 'none' && pdfs.length === 0 && htmls.length === 0) {
    firstFile = file
  }

  if (file.indexOf('pdf') === 0) {
    pdfs = R.append(file, pdfs);
  }

  if (file.indexOf('html') === 0) {
    htmls = R.append(file, htmls);
  }
}

function printProcessedFile() {
  console.log('File processed: ', processingFile);
}

export function processFromQueue() {
  timeout = 100;

  switch(priority) {
    case 'pdf':
      // search for a pdf
      if (pdfs.length > 0) {
        processingFile = pdfs[0];
        pdfs = R.remove(0, 1, pdfs);
        priority = 'html';
        numberOfFiles = 0;
        timeout = 5000;
        printProcessedFile();
      // if no pdfs, process html, but keep pdf priority
      } else if (htmls.length >= 0) {
        processingFile = htmls[0];
        htmls = R.remove(0, 1, htmls);
        priority = 'pdf';
        numberOfFiles = 0;
        timeout = 1000;
        printProcessedFile();
      }
      break;
    case 'html':
      // search for an html
      if (htmls.length > 0) {
        processingFile = htmls[0];
        htmls = R.remove(0, 1, htmls);
        numberOfFiles++;
        priority = numberOfFiles >= 5 ? 'pdf' : 'html';
        numberOfFiles = priority === 'pdf' ? 0 : numberOfFiles;
        timeout = 1000;
        printProcessedFile();
      // if no htmls, process pdf, but keep html priority
      } else if (pdfs.length > 0) {
        processingFile = pdfs[0];
        pdfs = R.remove(0, 1, pdfs);
        priority = 'html';
        numberOfFiles = 0;
        timeout = 5000;
        printProcessedFile();
      }
      break;
    case 'none':
      processingFile = firstFile;

      if (processingFile.indexOf('pdf') === 0) {
        pdfs = R.remove(0, 1, pdfs);
        priority = 'html';
        numberOfFiles = 0;
        timeout = 5000;
        printProcessedFile();
      } else if(processingFile.indexOf('html') === 0) {
        htmls = R.remove(0, 1, htmls);
        priority = 'html';
        numberOfFiles++;
        timeout = 1000;
        printProcessedFile();
      }
      break;
  }

  setTimeout(processFromQueue, timeout);
}
