# Tell us about one of your commercial projects with Node.js and/or AngularJS.

## Introduction
The most important project on which I worked using Node.js represents a tourism platform, similar to Airbnb, which is used to manage a set of apartments and reservations, to deal with situations like overbooking and to also manage things like cleaning companies and apartment contracts, calculate prices, based on different algorithms, integrate with 3rd party platform, for registering the tourists, store the credit cards in a secure manner and do online payments.

## User Management
The platform has multiple roles, such as: apartment owner, guest, operations, which can all access different information, and presents a system of permissions in order to manage the individual access rights for each user.

## Database
The database is managed using Postgresql, and currently amounts a total of 40 different tables, 20 views, and many stored functions and triggers. In addition, the database has history tables for the most important entities of the platform.

## Back-end
The back-end is implemented in Node.js, using Express and npm as package manager. It is integrated with multiple services, some developed within the company and other which are offered by 3rd party entities. The back-end also relies on cron jobs in order to perform automatic actions at regular intervals.

## Front-end
The front-end is developed using React (no Angular, I'm afraid), and it is composed from views, forms, interactive tables, validation rules, reducers for most of the entities presented on the platform.

## My roles
My role in the development of the platform involves tasks both on the back-end and on the front-end, and I was responsible for developing several modules of the platform, such as the management of the apartment contracts with the owners. Also, my work involves the creation of several cron jobs, and of some services, such as the one responsible for retrieving the data from Booking.com API. During my time in the company, I was also responsible for implementing the credit cards management part, which relies on Stripe for storing the credit cards and performing credit card operations.
