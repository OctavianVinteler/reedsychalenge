# README #

### What is this repository for? ###

The chalenge from Reedsy

### How do I get set up? ###

* `npm install`
* `npm start`

### Queue server ###

* `/fileProcessor/` for processing individual files

  Body:  `{
    	"file": "pdf #1"
    }`

* `/fileSetProcessor/` for processing a set of files

  Body:  `{
    	"files": ["pdf #1", "pdf #2", "html #3", "html #4", "html #5", "html #6", "html #7", "html #8", "pdf #9", "html #10"]
    }`
