# Detail how would you persist in data/present a schema to store several versioned text-based documents.

## Observation 1
Depending on the size of the documents, I would also consider storing the actual document using a commercial storage service, such as Amazon or Google and replace the ```version_content``` with a ```version_address``` which would contain a link to the actual document in order to avoid overloading the database with the large text field. But since it was specified that the documents contain only text, this would be my first option.

## Observation 2
I am aware that the ```current_version``` column from the ```document``` table is a redundant link to the ```version``` table, since the ```version``` already contains a link to the ```document``` table, but I would add this column for easy and fast access to the current version of the document, since it is likely that this would be a very common use case (to get the current version of a document).
