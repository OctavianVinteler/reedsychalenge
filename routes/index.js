import R             from 'ramda';
import * as express  from 'express';
import { addToTheQueue } from '../processors/fileProcessor';
import path from 'path';

let router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/dates/', (req, res) => {
  var filePath = "./views/dates.html"
  var resolvedPath = path.resolve(filePath);
  res.sendFile(resolvedPath);
});

router.post('/fileProcessor/', (req, res) => {
  const { file } = req.body;

  if (R.isNil(file) || R.isEmpty(file)) {
    res.sendStatus(400);
  }
  else if (file.indexOf('pdf') != 0 && file.indexOf('html') != 0) {
    res.sendStatus(400);
  }
  try {
    addToTheQueue(file);
  } catch (e) {
    console.log('error', e);
  }

  res.sendStatus(200);
});

router.post('/fileSetProcessor/', (req, res) => {
  const { files } = req.body;

  if (R.isNil(files) || R.type(files) !== 'Array') {
    res.sendStatus(400);
  }

  R.forEach(addToTheQueue,
    R.filter((file) => file.indexOf('pdf') === 0 || file.indexOf('html') === 0,
    files));

  res.sendStatus(200);
});

module.exports = router;
